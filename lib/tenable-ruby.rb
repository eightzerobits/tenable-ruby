#!/usr/bin/env ruby
# coding: utf-8
# = tenable-ruby.rb: Ruby library for communicating with the tenable.io API
#
#
# (C) Copyright (c) 2010 Vlatko Kosturjak, Kost, 2019 Intruder Systems Ltd. Distributed under MIT license.
# 
# == What is this library? 
# 
# Ruby library for communicating with the tenable.io API.
# You can start, stop, pause and resume scans. Get status of scans, download reports, create policies, etc.
#
# == Requirements
# 
# Standard Ruby libraries: uri, net/https and json.
#

require 'openssl'
require 'uri'
require 'net/http'
require 'net/https'
require 'json'
require 'error/authentication_error'
require 'error/tenable_error'

module TenableRuby
  class Client
    attr_accessor :quick_defaults
    attr_accessor :defsleep, :httpsleep, :httpretry, :ssl_use, :ssl_verify, :autologin
    attr_reader :header

    class << self
      @connection
    end

    # initialize quick scan defaults: these will be used when not specifying defaults
    #
    # Usage: 
    # 
    #  n.init_quick_defaults()
    def init_quick_defaults
      @quick_defaults = Hash.new
      @quick_defaults['enabled'] = false
      @quick_defaults['launch'] = 'ONETIME'
      @quick_defaults['launch_now'] = true
      @quick_defaults['description'] = 'Created with tenable-ruby https//gitlab.com/intruder/tenable-ruby'
    end

    # initialize object: try to connect to tenable.io
    # Usage:
    #
    #  TenableRuby::Client.new (:credentials => {username: 'user', password: 'password'})
    #  or
    #  TenableRuby::Client.new (:credentials => {access_key: 'XXX', secret_key: 'XXX'})
    #
    def initialize(params = {})
      # defaults
      @tenable_url = params.fetch(:url, 'https://cloud.tenable.com')
      @credentials = params.fetch(:credentials)
      @ssl_verify = params.fetch(:ssl_verify, false)
      @ssl_use = params.fetch(:ssl_use, true)
      @autologin = params.fetch(:autologin, true)
      @defsleep = params.fetch(:defsleep, 1)
      @httpretry = params.fetch(:httpretry, 1)
      @httpsleep = params.fetch(:httpsleep, 1)

      init_quick_defaults

      uri = URI.parse(@tenable_url)
      @connection = Net::HTTP.new(uri.host, uri.port)
      @connection.use_ssl = @ssl_use

      if @ssl_verify
        @connection.verify_mode = OpenSSL::SSL::VERIFY_PEER
      else
        @connection.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end

      yield @connection if block_given?
      authenticate if @autologin
    end

    # Tries to authenticate to the tenable.io REST JSON interface using username/password or API keys
    def authenticate
      if @credentials[:username] and @credentials[:password]
        payload = {
          :username => @credentials[:username],
          :password => @credentials[:password],
          :json => 1,
          :authenticationmethod => true
        }
        response = http_post(:uri => "/session", :data => payload)
        if response['token']
          @token = "token=#{response['token']}"
          @header = {'X-Cookie' => @token}
        else
          raise TenableRuby::Error::AuthenticationError, "Authentication failed. Could not authenticate using
          username/password."
        end
      elsif @credentials[:access_key] and @credentials[:secret_key]
        @header = {'X-ApiKeys' => "accessKey=#{@credentials[:access_key]}; secretKey=#{@credentials[:secret_key]}"}
      else
        raise TenableRuby::Error::AuthenticationError, "Authentication credentials were not provided. You must " \
        "provide either a username and password or an API access key and secret key (these can be generated at " \
        "https://cloud.tenable.com/app.html#/settings/my-account/api-keys."
      end
    end

    # Returns the server version and other properties
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/server/properties
    def get_server_properties
      http_get(:uri => "/server/properties", :fields => header)
    end

    # Creates a new user
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/users/create
    def user_add(username, password, permissions, type)
      payload = {
        :username => username,
        :password => password,
        :permissions => permissions,
        :type => type,
        :json => 1
      }
      http_post(:uri => "/users", :fields => header, :data => payload)
    end

    # Deletes a user
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/users/delete
    def user_delete(user_id)
      response = http_delete(:uri => "/users/#{user_id}", :fields => header)
      response.code
    end

    # Changes the password for the given user
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/users/password
    def user_chpasswd(user_id, password)
      payload = {
        :password => password,
        :json => 1
      }
      response = http_put(:uri => "/users/#{user_id}/chpasswd", :data => payload, :fields => header)
      response.code
    end

    # Logs the current user out and destroys the session
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/session/destroy
    def user_logout
      response = http_delete(:uri => "/session", :fields => header)
      response.code
    end

    # Returns the policy list
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/policies/list
    def list_policies
      http_get(:uri => "/policies", :fields => header)
    end

    # Returns the user list
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/users/list
    def list_users
      http_get(:uri => "/users", :fields => header)
    end

    # Returns the current user's scan folders
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/folders/list
    def list_folders
      http_get(:uri => "/folders", :fields => header)
    end

    # Returns the scanner list
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scanners/list
    def list_scanners
      http_get(:uri => "/scanners", :fields => header)
    end

    # Returns the list of plugin families
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/plugins/families
    def list_families
      http_get(:uri => "/plugins/families", :fields => header)
    end

    # Returns the list of plugins in a family
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/plugins/family-details
    def list_plugins(family_id)
      http_get(:uri => "/plugins/families/#{family_id}", :fields => header)
    end

    # Returns the template list
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/editor/list
    def list_templates(type)
      http_get(:uri => "/editor/#{type}/templates", :fields => header)
    end

    # Returns details for the given template
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/editor/template-details
    def editor_templates (type, uuid)
      http_get(:uri => "/editor/#{type}/templates/#{uuid}", :fields => header)
    end

    # Returns details for a given plugin
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/plugins/plugin-details
    def plugin_details(plugin_id)
      http_get(:uri => "/plugins/plugin/#{plugin_id}", :fields => header)
    end

    # Returns the server status
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/server/status
    def server_status
      http_get(:uri => "/server/status", :fields => header)
    end

    # Creates a scan
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/create
    def scan_create(uuid, settings)
      payload = {
        :uuid => uuid,
        :settings => settings,
        :json => 1
      }.to_json
      http_post(:uri => "/scans", :body => payload, :fields => header, :ctype => 'application/json')
    end

    # Launches a scan
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/launch
    def scan_launch(scan_id)
      http_post(:uri => "/scans/#{scan_id}/launch", :fields => header)
    end

    # Get List of Scans
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/list
    def scan_list
      http_get(:uri => "/scans", :fields => header)
    end

    # Returns details for the given scan
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/details
    def scan_details(scan_id)
      http_get(:uri => "/scans/#{scan_id}", :fields => header)
    end

    # Pauses a scan
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/pause
    def scan_pause(scan_id)
      http_post(:uri => "/scans/#{scan_id}/pause", :fields => header)
    end

    # Resumes a scan
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/resume
    def scan_resume(scan_id)
      http_post(:uri => "/scans/#{scan_id}/resume", :fields => header)
    end

    # Stops a scan
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/stop
    def scan_stop(scan_id)
      http_post(:uri => "/scans/#{scan_id}/stop", :fields => header)
    end

    # Export the given scan. Once requested, the file can be downloaded using the export download method
    # upon receiving a "ready" status from the export status method.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/export-request
    def scan_export(scan_id, format)
      payload = {
        :format => format
      }.to_json
      http_post(:uri => "/scans/#{scan_id}/export", :body => payload, :ctype => 'application/json', :fields => header)
    end

    # Check the file status of an exported scan. When an export has been requested, it is necessary to poll this
    # endpoint until a "ready" status is returned, at which point the file is complete and can be downloaded
    # using the export download endpoint.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/export-status
    def scan_export_status(scan_id, file_id)
      http_get(:uri => "/scans/#{scan_id}/export/#{file_id}/status", :fields => header)
    end

    # Deletes a scan. NOTE: Scans in running, paused or stopping states can not be deleted.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/delete
    def scan_delete(scan_id)
      response = http_delete(:uri => "/scans/#{scan_id}", :fields => header)
      return response.code == 200
    end

    # Returns details for the given host
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/host-details
    def host_details(scan_id, host_id, history_id: nil)
      uri = "/scans/#{scan_id}/hosts/#{host_id}"
      unless history_id.nil?
        uri += "?history_id=#{history_id}"
      end
      http_get(:uri => uri, :fields => header)
    end

    # Download an exported scan
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/export-download
    def report_download(scan_id, file_id)
      http_get(:uri => "/scans/#{scan_id}/export/#{file_id}/download", :raw_content => true, :fields => header)
    end

    # Returns details for the given policy
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/scans/host-details
    def policy_details(policy_id)
      http_get(:uri => "/policies/#{policy_id}", :fields => header)
    end

    # Creates a policy
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/policies/create
    def policy_create(template_id, plugins, settings)
      options = {
        :uri => "/policies/",
        :fields => header,
        :ctype => 'application/json',
        :body => {
          :uuid => template_id,
          :audits => {},
          :credentials => {delete: []},
          :plugins => plugins,
          :settings => settings
        }.to_json
      }
      http_post(options)
    end

    # Copy a policy
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/policies/copy
    def policy_copy(policy_id)
      options = {
        :uri => "/policies/#{policy_id}/copy",
        :fields => header,
        :ctype => 'application/json'
      }
      http_post(options)
    end

    # Changes the parameters of a policy
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/policies/configure
    def policy_configure(policy_id, template_id, plugins, settings)
      options = {
        :uri => "/policies/#{policy_id}",
        :fields => header,
        :ctype => 'application/json',
        :body => {
          :uuid => template_id,
          :audits => {},
          :credentials => {delete: []},
          :plugins => plugins,
          :settings => settings
        }.to_json
      }
      http_put(options)
    end

    # Delete a policy
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/policies/delete
    def policy_delete(policy_id)
      response = http_delete(:uri => "/policies/#{policy_id}", :fields => header)
      response.code
    end

    # Performs scan with templatename provided (name, title or uuid of scan).
    # Name is your scan name and targets are targets for scan
    #
    # returns: JSON parsed object with scan info
    def scan_quick_template (templatename, name, targets)
      templates = list_templates('scan')['templates'].select do |temp|
        temp['uuid'] == templatename or temp['name'] == templatename or temp['title'] == templatename
      end
      if templates.nil?
        return nil
      end
      template_uuid = templates.first['uuid']
      settings = editor_templates('scan', template_uuid)
      settings.merge!(@quick_defaults)
      settings['name'] = name
      settings['text_targets'] = targets
      scan_create(template_uuid, settings)
    end

    # Performs scan with scan policy provided (uuid of policy or policy name).
    # Name is your scan name and targets are targets for scan
    # (foldername is optional - folder where to save the scan (if that folder exists))
    # (scanner_id is optional - ID of the scanner/cloud scanner you want to run this scan on)
    #
    # returns: JSON parsed object with scan info
    def scan_quick_policy (policyname, name, targets, foldername = nil, scanner_id = nil)
      policies = list_policies['policies'].select do |pol|
        pol['id'] == policyname or pol['name'] == policyname
      end
      if policies.size == 0
        return nil
      end
      policy = policies.first
      template_uuid = policy['template_uuid']
      settings = Hash.new
      settings.merge!(@quick_defaults)
      settings['name'] = name
      settings['policy_id'] = policy['id']
      settings['text_targets'] = targets
      unless foldername.nil?
        folders = list_folders['folders'].select do |folder|
          folder['name'] == foldername
        end
        unless folders.size == 0
          settings['folder_id'] = folders.first['id']
        end
      end
      unless scanner_id.nil?
        settings['scanner_id'] = scanner_id
      end
      scan_create(template_uuid, settings)
    end

    # Returns scan status by performing a 'scan_details' API call
    def scan_status(scan_id)
      details = scan_details(scan_id)
      unless details['error'].nil?
        return 'error'
      end
      if details.nil?
        return 'error'
      end
      details['info']['status']
    end

    # Returns the status of the latest history object of a scan by performing a 'scan_details' API call.
    # Note this is currently updated more frequently than the scan status in the tenable.io API
    def scan_latest_history_status(scan_id)
      details = scan_details(scan_id)
      unless details['error'].nil?
        return 'error'
      end
      if details.nil?
        return 'error'
      end
      history = details['history']
      if history.nil? or history.length == 0
        'error'
      else
        details['history'].last['status']
      end
    end

    # Parse the scan status command to determine if a scan has finished
    def scan_finished?(scan_id)
      status = scan_status(scan_id)
      if status == 'completed' or status == 'canceled' or status == 'imported'
        true
      else
        false
      end
    end

    # use download scan API call to download a report in raw format
    def report_download_quick(scan_id, format)
      export_details = scan_export(scan_id, format)
      # ready, loading
      while (export_status = scan_export_status(scan_id, export_details['file'])['status']) != "ready" do
        if export_status.nil? or export_status == '' or export_status == "error"
          raise TenableRuby::Error::TenableError, "Tenable.io returned an error while exporting the scan"
        end
        sleep @defsleep
      end
      report_download(scan_id, export_details['file'])
    end

    # use download scan API call to save a report as file
    def report_download_file(scan_id, format, output_file_name)
      report_content = report_download_quick(scan_id, format)
      File.open(output_file_name, 'w') do |f|
        f.write(report_content)
      end
    end

    # Returns a list of all containers.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/container-security-containers/list-containers
    def list_containers()
      http_get(:uri => "/container-security/api/v1/container/list", :fields => header)
    end

    # Returns an inventory of an image by ID.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/container-security-containers/image-inventory
    def image_inventory(image_id)
      http_get(:uri => "/container-security/api/v1/container/#{policy_id}/status", :fields => header)
    end

    # Returns the status of a job that you specify by ID to determine if the job is still queued, in progress, or has completed.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/container-security-jobs/job-status
    def job_status(job_id)
      http_get(:uri => "/container-security/api/v1/jobs/status?job_id=#{job_id}", :fields => header)
    end

    # Returns the status of a job by specifying an image ID to determine if the job is still queued, in progress, or has completed.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/container-security-jobs/job-status-by-image-id
    def image_status(image_id)
      http_get(:uri => "/container-security/api/v1/jobs/image_status?image_id=#{image_id}", :fields => header)
    end

    # Returns the status of a job by specifying an image digest to determine if the job is still queued, in progress, or has completed.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/container-security-jobs/job-status-by-image-digest
    def image_status_digest(image_digest)
      http_get(:uri => "/container-security/api/v1/jobs/image_status_digest?image_digest=#{image_digest}", :fields => header)
    end

    # Returns a list of active Container Security jobs
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/container-security-jobs/list-jobs
    def list_jobs()
      http_get(:uri => "/container-security/api/v1/jobs/list", :fields => header)
    end

    # Checks the compliance of an image that you specify by ID against your policies.
    #
    # Reference: 
    # https://cloud.tenable.com/api#/resources/container-security-policy/policy-compliance-by-id
    def policy_compliance_by_id(image_id)
      http_get(:uri => "/container-security/api/v1/policycompliance?image_id=#{image_id}", :fields => header)
    end

    # Checks the compliance of an image that you specify by name against your policies.
    #
    # Reference: 
    # https://cloud.tenable.com/api#/resources/container-security-policy/policy-compliance-by-name
    def policy_compliance_by_name(image)
      http_get(:uri => "/container-security/api/v1/compliancebyname?image=#{image}", :fields => header)
    end

    # Returns a report in JSON format for a container that you specify by ID. Note: If you do not have the container_id, you can call the list-containers endpoint.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/container-security-reports/report-by-container-id
    def report_by_container_id(container_id)
      http_get(:uri => "/container-security/api/v1/reports/show?container_id=#{container_id}", :fields => header)
    end

    # Returns a report in JSON format for an image that you specify by ID. Note: If you do not have the image_id, you can call the list-images endpoint.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/container-security-reports/report-by-image-id
    def report_by_image_id(image_id)
      http_get(:uri => "/container-security/api/v1/reports/by_image?image_id=#{image_id}", :fields => header)
    end

    # Returns a report in JSON format for an image digest.
    #
    # Reference:
    # https://cloud.tenable.com/api#/resources/container-security-reports/report-by-image-digest
    def report_by_image_digest(image_digest)
      http_get(:uri => "/container-security/api/v1/reports/by_image_digest?image_digest=#{image_digest}", :fields => header)
    end

    private

    # Perform HTTP put method with uri, data and fields
    #
    # returns: HTTP result object
    def http_put(opts = {})
      response = http_put_low(opts)
      if response.is_a?(Hash) and response.has_key?('error') and response['error'] == 'Invalid Credentials'
        authenticate
        http_put_low(opts)
      else
        response
      end
    end

    def http_put_low(opts = {})
      uri = opts[:uri]
      data = opts[:data]
      fields = opts[:fields] || {}
      response = nil
      tries = @httpretry

      request = Net::HTTP::Put.new(uri)
      request.set_form_data(data) unless (data.nil? || data.empty?)
      fields.each_pair do |name, value|
        request.add_field(name, value)
      end

      begin
        tries -= 1
        response = @connection.request(request)
      rescue Timeout::Error, Errno::EINVAL, Errno::ECONNRESET, EOFError, Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError => e
        if tries > 0
          sleep @httpsleep
          retry
        else
          return response
        end
      rescue URI::InvalidURIError
        return response
      end
    end

    # Perform HTTP delete method with uri, data and fields
    #
    # returns: HTTP result object
    def http_delete(opts = {})
      response = http_delete_low(opts)
      if response.is_a?(Hash) and response.has_key?('error') and response['error'] == 'Invalid Credentials'
        authenticate
        http_delete_low(opts)
        response
      else
        response
      end
    end

    def http_delete_low(opts = {})
      uri = opts[:uri]
      fields = opts[:fields] || {}
      response = nil
      tries = @httpretry

      request = Net::HTTP::Delete.new(uri)

      fields.each_pair do |name, value|
        request.add_field(name, value)
      end

      begin
        tries -= 1
        response = @connection.request(request)
      rescue Timeout::Error, Errno::EINVAL, Errno::ECONNRESET, EOFError, Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError => e
        if tries > 0
          sleep @httpsleep
          retry
        else
          return response
        end
      rescue URI::InvalidURIError
        return response
      end
    end

    # Perform HTTP get method with uri and fields
    #
    # returns: JSON parsed object (if JSON parseable)
    def http_get(opts = {})
      raw_content = opts[:raw_content] || false
      response = http_get_low(opts)
      if !raw_content
        if response.is_a?(Hash) and response.has_key?('error') and response['error'] == 'Invalid Credentials'
          authenticate
          response = http_get_low(opts)
          return response
        else
          return response
        end
      else
        response
      end
    end

    def http_get_low(opts = {})
      uri = opts[:uri]
      fields = opts[:fields] || {}
      raw_content = opts[:raw_content] || false
      json = {}
      tries = @httpretry

      request = Net::HTTP::Get.new(uri)
      fields.each_pair do |name, value|
        request.add_field(name, value)
      end

      begin
        tries -= 1
        response = @connection.request(request)
      rescue Timeout::Error, Errno::EINVAL, Errno::ECONNRESET, EOFError, Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError => e
        if tries > 0
          sleep @httpsleep
          retry
        else
          return json
        end
      rescue URI::InvalidURIError
        return json
      end
      if !raw_content
        parse_json(response.body)
      else
        response.body
      end
    end

    # Perform HTTP post method with uri, data, body and fields
    #
    # returns: JSON parsed object (if JSON parseable)
    def http_post(opts = {})
      if opts.has_key?(:authenticationmethod)
        # i know authzmethod = opts.delete(:authorizationmethod) is short, but not readable
        authzmethod = opts[:authenticationmethod]
        opts.delete(:authenticationmethod)
      end
      response = http_post_low(opts)
      if response.is_a?(Hash) and response.has_key?('error') and response['error'] == 'Invalid Credentials'
        unless authzmethod
          authenticate
          response = http_post_low(opts)
          return response
        end
      else
        response
      end
    end

    def http_post_low(opts = {})
      uri = opts[:uri]
      data = opts[:data]
      fields = opts[:fields] || {}
      body = opts[:body]
      ctype = opts[:ctype]
      json = {}
      tries = @httpretry

      request = Net::HTTP::Post.new(uri)
      request.set_form_data(data) unless (data.nil? || data.empty?)
      request.body = body unless (body.nil? || body.empty?)
      request['Content-Type'] = ctype unless (ctype.nil? || ctype.empty?)
      fields.each_pair do |name, value|
        request.add_field(name, value)
      end

      begin
        tries -= 1
        response = @connection.request(request)
      rescue Timeout::Error, Errno::EINVAL, Errno::ECONNRESET, EOFError, Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError => e
        if tries > 0
          sleep @httpsleep
          retry
        else
          return json
        end
      rescue URI::InvalidURIError
        return json
      end

      parse_json(response.body)
    end

    # Perform JSON parsing of body
    #
    # returns: JSON parsed object (if JSON parseable)
    def parse_json(body)
      parsed_json = {}

      begin
        parsed_json = JSON.parse(body)
      rescue JSON::ParserError
      end

      parsed_json
    end

  end
end

