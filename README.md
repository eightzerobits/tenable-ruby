# tenable-ruby

Unofficial Ruby library for communicating with the [tenable.io](https://www.tenable.com/products/tenable-io) API

## Quick usage example

```ruby
require 'tenable-ruby'

# initialise the client
tenable_client = TenableRuby::Client.new({access_key: 'XXX', secret_key: 'XXX'})
# kick off the scan (assuming you've already created a policy in tenable.io)
scan_details = tenable_client.scan_quick_policy('policy name', 'scan name', 'comma separate list of targets')
# grab the scan_id
scan_id = scan_details['scan']['id']
# check whether the scan has finished
if tenable_client.scan_finished?(scan_id)
  # export the scan results as .nessus (you can also get the results using the API)
  tenable_client.report_download_file(scan_id, 'nessus', 'myscanreport.nessus')  
end
```

## Installation

Add this line to your application's Gemfile:

    gem 'tenable-ruby'

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install tenable-ruby

## Requirements

Requirements are the standard Ruby libraries for HTTPS and JSON
parsing:
```ruby
require 'uri'
require 'net/https'
require 'json'
```

## Copyright
Copyright (c) 2010 Vlatko Kosturjak, 2019 Intruder Systems Ltd. See LICENSE.txt for further details.