## 0.2.9

### Changed

- in `report_download_quick` raise `TenableError` if export status is `nil`, `""` or `"error"`

### Other

- Clean up README, add CHANGELOG and CONTRIBUTING
- Remove references to Nessus Professional, API has been deactivated in latest version
- Clean up code

