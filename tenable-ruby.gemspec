# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name = 'tenable-ruby'
  spec.homepage = 'https://gitlab.com/intruder/tenable-ruby'
  spec.license = 'MIT'
  spec.summary = %Q{Ruby library for communicating with the tenable.io API}
  spec.description = %Q{Ruby library for communicating with the tenable.io API.
  You can start, stop, pause and resume scan. Get status of scans, download reports, create policies, etc.}
  spec.email = 'patrick.craston@intruder.io'
  spec.authors = ['Vlatko Kosturjak', 'Patrick Craston']
  spec.version = File.read('VERSION')
  spec.files = ["lib/tenable-ruby.rb", "lib/error/authentication_error.rb", "lib/error/tenable_error.rb"]
end
